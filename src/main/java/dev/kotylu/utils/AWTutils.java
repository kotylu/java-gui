package dev.kotylu.utils;

import java.awt.*;

public class AWTutils {
    public static Component getComponentByName(Component[] components, String name) {
        for (Component component : components) {
            if (component.getName().equalsIgnoreCase(name)) {
                return component;
            }
        }
        return null;
    }
}
