package dev.kotylu.models;

public class Vector {
    private int x;
    private int y;

    public Vector(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Vector() {
        this.x = 0;
        this.y = 0;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Vector subtract(Vector sub) {
        return new Vector(this.x - sub.x, this.y - sub.y);
    }
    public static Vector subtract(Vector st, Vector nd) {
        return new Vector(st.x - nd.x, st.y - nd.y);
    }

    public Vector add(Vector sub) {
        return new Vector(this.x + sub.x, this.y + sub.y);
    }
    public static Vector add(Vector st, Vector nd) {
        return new Vector(st.x + nd.x, st.y + nd.y);
    }
}
