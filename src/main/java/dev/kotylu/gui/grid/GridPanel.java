package dev.kotylu.gui.grid;

import dev.kotylu.gui.containers.ComponentHandler;
import dev.kotylu.gui.containers.PanelContainer;
import dev.kotylu.models.Vector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;

public class GridPanel extends PanelContainer implements ComponentHandler {
    public static Logger log = LoggerFactory.getLogger(GridPanel.class);
    private Dimension gridSize;
    private int[][] grid;
    private int gridSpace = 10;

    public GridPanel(int gridX, int gridY) {
        gridSize = new Dimension(gridX, gridY);
        setVisible(true);
        setBackground(Color.BLACK);
        grid = new int[gridSize.width][gridSize.height];

    }

    public void afterAdding() {
        this.setLayout(null);
        setPosition(new Vector());
        setSize(getParent().getSize());
        Dimension gridItemDimension = new Dimension(
                ((getParent().getSize().width - ((gridSpace + 1) * gridSize.width)) / (gridSize.width)),
                ((getParent().getSize().height - ((gridSpace + 1) * gridSize.height)) / (gridSize.height))
        );
        log.info("start creating grid");
        for (int x = 0; x < gridSize.width; x++) {
            for (int y = 0; y < gridSize.height; y++) {
                PanelContainer panelContainer = new PanelContainer(
                        (x*gridItemDimension.width)+x*this.gridSpace+this.gridSpace,
                        (y*gridItemDimension.height)+y*this.gridSpace+this.gridSpace
                );
                panelContainer.setName(x+";"+y);
                panelContainer.setBackground(Color.WHITE);
                panelContainer.setSize(gridItemDimension);
                panelContainer.addMouseListener(new GridTileHandler());
                this.add(panelContainer);
            }
        }
        log.info("end creating grid");
    }

    @Override
    public Component getComponent() {
        return this;
    }
}
