package dev.kotylu.gui.grid;

import dev.kotylu.gui.containers.PanelContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class GridTileHandler implements MouseListener {
    public static Logger log = LoggerFactory.getLogger(GridTileHandler.class);

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        PanelContainer caller = (PanelContainer) mouseEvent.getSource();
        log.warn("{}, Clicked!", caller.getName());
        log.warn("{} {}!", mouseEvent.getX(), mouseEvent.getY());
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
