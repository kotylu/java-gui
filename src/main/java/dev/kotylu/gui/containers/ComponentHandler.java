package dev.kotylu.gui.containers;

import java.awt.*;

public interface ComponentHandler {
    Component getComponent();
    void afterAdding();
}
