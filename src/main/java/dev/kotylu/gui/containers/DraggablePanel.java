package dev.kotylu.gui.containers;

import java.awt.*;

import dev.kotylu.models.Vector;
import org.slf4j.*;
import java.awt.event.*;

/**
 * DraggablePanel object that can be moved by dragging
 * from position A to position B
 *
 * to change position use setPosition
 */
public class DraggablePanel extends PanelContainer implements MouseListener, MouseMotionListener, ComponentHandler {
    public Logger log = LoggerFactory.getLogger(DraggablePanel.class);
    private Vector dragStartScreenPosition;
    private DraggablePanel dragingShadow;

    /**
     * creates new instance with copied visual properties from provided Object
     *
     * @param src source object to create copy from
     * @return new instance of DraggablePanel with same visual props
     */
    public static DraggablePanel makeCopy(DraggablePanel src) {
        DraggablePanel copy = new DraggablePanel();
        copy.setBackground(src.getBackground());
        copy.setPosition(src.getPosition());
        copy.setSize(src.getSize());
        return copy;
    }

    public DraggablePanel() {
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        afterAdding();
    }

    @Override
    public void afterAdding() {
        this.setPosition(new Vector());
        this.setSize(new Dimension(100, 150));
    }

    /**
     * this event handles start of drag
     *
     * crates copy and sets it up as shadow
     * saves starting position of the drag
     * @param mouseEvent
     */
    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        this.dragStartScreenPosition = new Vector(mouseEvent.getXOnScreen(), mouseEvent.getYOnScreen());
        log.info("drag start: X, Y: {}, {}", mouseEvent.getXOnScreen(), mouseEvent.getY());
        this.dragingShadow = DraggablePanel.makeCopy(this);
        Color srcColor = this.getBackground();
        this.dragingShadow.setBackground(new Color(srcColor.getRed(), srcColor.getGreen(), srcColor.getBlue(), 99));
        this.getParent().add(this.dragingShadow);
    }

    /**
     * this event handles end of drag
     *
     * calculates difference between start and end position and move this object to that calculated position
     * deletes copy that was simulating shadow and removes it
     * @param mouseEvent
     */
    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        log.info("drag end: X, Y: {}, {}", mouseEvent.getXOnScreen(), mouseEvent.getY());
        Vector dragEndScreenPosition = new Vector(mouseEvent.getXOnScreen(), mouseEvent.getYOnScreen());
        Vector movedBy = Vector.subtract(this.dragStartScreenPosition, dragEndScreenPosition);
        Vector newPosition = Vector.subtract(this.getPosition(), movedBy);
        this.setPosition(newPosition);
        this.getParent().remove(this.dragingShadow);
        this.dragingShadow = null;
    }

    /**
     * this event handles shadow affect
     *
     * takes care of moving object simulating shadow
     * @param mouseEvent
     */
    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        //dragged - show shadow
        Vector dragEndScreenPosition = new Vector(mouseEvent.getXOnScreen(), mouseEvent.getYOnScreen());
        Vector movedBy = Vector.subtract(this.dragStartScreenPosition, dragEndScreenPosition);
        Vector newPosition = Vector.subtract(this.getPosition(), movedBy);
        this.dragingShadow.setPosition(newPosition);
    }

    /** not implemented */
    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
        log.debug("not implemented");
    }

    /** not implemented */
    @Override
    public void mouseExited(MouseEvent mouseEvent) {
        log.debug("not implemented");
    }

    /** not implemented */
    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        log.debug("not implemented");
    }

    /** not implemented */
    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        log.debug("not implemented");
    }

    @Deprecated
    /**
     * You should not be able to change size when changing position
     * and same the other way
     *
     * use setPosition instead
     */
    public void setBounds(int x, int y, int width, int height) {
        super.setBounds(x, y, width, height);
    }

    @Override
    public Component getComponent() {
        return this;
    }
}
