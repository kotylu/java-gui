package dev.kotylu.gui.containers;

import dev.kotylu.models.Vector;

import java.awt.*;

public class PanelContainer extends Panel {
    private Dimension size;
    private Vector position;

    public PanelContainer() {
        size = new Dimension(100, 100);
        setSize(size.width, size.height);
        setVisible(true);
        setPosition(new Vector());
    }
    public PanelContainer(int x, int y) {
        size = new Dimension(100, 100);
        setSize(size.width, size.height);
        setVisible(true);
        setPosition(new Vector(x, y));
    }

    public void setPosition(Vector position) {
        this.setBounds(position.getX(), position.getY(), this.size.width, this.size.height);
        this.position = position;
    }

    public Vector getPosition() {
        return this.position;
    }

    @Deprecated
    public void setBounds(int x, int y, int width, int height) {
        super.setBounds(x, y, width, height);
    }

    @Deprecated
    public void setBounds(Rectangle rect) {
        super.setBounds(rect);
    }



}
