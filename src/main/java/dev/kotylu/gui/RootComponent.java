package dev.kotylu.gui;

import dev.kotylu.gui.containers.ComponentHandler;
import dev.kotylu.gui.containers.DraggablePanel;
import dev.kotylu.gui.grid.GridPanel;
import dev.kotylu.models.Vector;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.*;
import java.awt.*;

/**
 * Main "window"
 * root container for awt components
 */
public class RootComponent extends Frame {
    public static Logger log = LoggerFactory.getLogger(RootComponent.class);

    private int width = 800;
    private int height = 600;
    private String title = "Hello Frame!";
    private List<ComponentHandler> components;

    public RootComponent() {
        this.initiate();
        //this.createBtnMoveObject();
        //this.createDraggablePanel();
        this.createGridPanel();

        for (ComponentHandler handler : this.components) {
            this.add(handler.getComponent());
            handler.afterAdding();
            log.info("Added component: {}", handler.getComponent().getName());
        }
    }

    private void initiate() {
        this.components = new ArrayList<>();
        setName("root");
        setSize(this.width, this.height);
        setTitle(this.title);
        // no layout manager
        setLayout(null);
        setVisible(true);
        log.info("Main Frame initialized");
    }

    /**
     * create new instance of BtnMoveObject
     * and adds it to component list
     */
    private void createBtnMoveObject() {
        BtnMoveObject btnMoveObject = new BtnMoveObject();
        btnMoveObject.setName("btn-move-object");
        btnMoveObject.setBounds(150, 150, 70, 30);
        log.info("Created button Move");
        this.components.add(btnMoveObject);
    }

    /**
     * create new instance of DraggablePanel
     * and adds it to component list
     */
    private void createDraggablePanel() {
        DraggablePanel draggablePanel = new DraggablePanel();
        draggablePanel.setName("draggable-panel");
        draggablePanel.setPosition(new Vector(400, 400));
        draggablePanel.setBackground(Color.MAGENTA);
        log.info("Created draggable panel (MAGENTA)");
        this.components.add(draggablePanel);
    }

    /**
     * create new instance of GridPanel
     * and adds it to component list
     */
    private void createGridPanel() {
        GridPanel gridPanel = new GridPanel(8,8);
        gridPanel.setName("grid-panel");
        log.info("Created grid panel");
        this.components.add(gridPanel);
    }

}
