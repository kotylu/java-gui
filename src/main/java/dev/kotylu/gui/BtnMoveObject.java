package dev.kotylu.gui;

import dev.kotylu.models.Vector;
import dev.kotylu.gui.containers.*;
import java.awt.event.*;
import org.slf4j.*;
import java.awt.*;

public class BtnMoveObject extends Button implements ActionListener, ComponentHandler {
    public static Logger log = LoggerFactory.getLogger(BtnMoveObject.class);
    private PanelContainer movablePanel;
    private int moveByValue = 25;
    private String label = "Move Object";

    public BtnMoveObject() {
        this.setLabel(label);
        this.addActionListener(this);
        this.movablePanel = new PanelContainer(100, 100);
        this.movablePanel.setBackground(Color.GREEN);
    }

    @Override
    /**
     * move movable panel by moveByValue
     * on x axis
     */
    public void actionPerformed(ActionEvent actionEvent) {
        Vector currPosition = movablePanel.getPosition();
        Vector newPosition = new Vector(currPosition.getX()+this.moveByValue, currPosition.getY());
        this.movablePanel.setPosition(newPosition);
        log.info("moving panel by {}", this.moveByValue);
    }

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public void afterAdding() {
        getParent().add(this.movablePanel);
    }
}
